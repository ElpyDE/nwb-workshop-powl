
__NWB Data Conversion Workshop 2024__

https://neurodatawithoutborders.github.io/nwb_hackathons/HCK19_2024_Remote/


# Project

__Owl Ephys data from Peña Lab__

## Key Investigators

*   Roland Ferger (roland.ferger@einsteinmed.edu)
*   Andrea J Bae
*   José L Peña

## Project Description

Our lab is studying sound localization in Barn Owls. We have
electrophysiological data recorded with different setups/software in our lab.

In our most recent experiments with awake owls and chronic Neuronexus implants,
we recorded 16 or 32 channels simultaneously (and continuously), with one or two
acoustic stimuli per trial from an array of 144 speakers. The experiment is run
with TDT3 and entirely controlled by our own Python software (pOwl). We save
data as HDF5 (organized closely following our own needs and performance
constraints).

Other experiments include data combined from two setups (TDT2 for acoustic
stimuli and Plexon Omniplex for multi-channel recording) controlled by separate
PCs and combined later according to event time stamps.

## Objectives

*   The main objective is to convert data for publishing, as mandated by at
    least one grant (Brain Initiative).
*   Future plans might include a “convert data early” strategy to be able to use
    NWB tools, replacing or complementing our existing data analysis pipelines
    (which would need to be rewritten as well).

## Approach and Plan

*   I want to work with our freshest datasets first (pOwl datasets). I hope that
    this is straight-forward once the NWB data structure becomes clearer to me
    and I know how to translate all data and metadata without losing any
    information along the way.

## Progress and Next Steps

*   Populate this section as you are making progress before/during/after the
    workshop. Describe the progress you have made on the project, e.g., which
    objectives you have achieved and how. Describe the next steps you are
    planning to take to complete the project.

## Materials

*   Links to materials relevant to the project, e.g., code or data. 
*   If available, add pictures and links to videos that demonstrate what has
    been accomplished.

## Background and References

*   Use this space for information that may help people better understand your
    project, like links to papers, source code, or data.
*   First project using pOwl is already published 😎
    https://doi.org/10.1523/JNEUROSCI.2081-23.2024
